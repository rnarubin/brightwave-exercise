//! An implementation of this programming exercise:
//!
//!> The task is to create a web crawler that exposes two distinct capabilities.  For the purposes of this exercise we can assume index is invoked before search, but we'll be interested to hear your thoughts on how this system could be designed such that search can be invoked while the indexer is active.
//!>
//!>  - index: Given a URL, initiate a web crawl to at most depth k, ensuring that you never crawl the same page twice. This method accepts two parameters, origin and k, where origin is a URL from which to initiate a web crawl and k is defined as the number of hops between origin and a newly-discovered link.  Please design around the assumption the scale of the crawl is very large but does not require multiple machines to run.
//!>
//!>  - search: Given a query, return all relevant URLs. This method accepts as input a string, query, and returns a list of triples, where each triple takes the form (relevant_url, origin_url, depth).  relevant_url is the URL of an indexed web page relevant to query, and origin and depth define parameters passed to /index for which relevant_url was discovered.  Feel free to make reasonable assumptions about the definition of relevancy for the purposes of this method
//!
//! ##
//! This solution runs a parallel web crawl during indexing by spinning up tokio tasks to fetch web
//! pages up to a configurable concurrency limit. Those pages are then fed to a thread pool which
//! builds an index over the words in the pages using a Finite State Transducer (which is a fancy
//! name for a trie-like map data structure, except both prefixes _and_ suffixes are shared among
//! keys). Search is then performed on the index by evaluting which pages contain words of
//! sufficiently small edit distance from the words in the query. More details are discussed in the
//! [`Index`] documentation.
//!
//! ## Possible Additions
//! To make this implementation more appropriate for a production workload, a few changes could be
//! considered:
//!  - The index/indices should be written to disk instead of held only in memory. This is helpful
//!  for two reasons: 1. disk space is larger than memory, allowing larger web crawls, 2.
//!  persisting the data during indexing would allow recovery in the event of machine failure.
//!  - Lexical word comparison does not consider the semantic meaning of words, which might
//!  surprise human users and limit the usefulness of the search. A more robust similarity metric
//!  should be considered.

use fst::{IntoStreamer, Streamer};
use futures::{stream::FuturesUnordered, Stream, StreamExt};
use regex::Regex;
use std::{
    collections::{BTreeMap, HashMap, HashSet, VecDeque},
    future::Future,
    mem,
    pin::pin,
    sync::Arc,
    time::Duration,
};
use tokio::time::error::Elapsed as TimeoutElapsed;
// turns out there's a nice crate that finds links in text
use linkify::LinkFinder;

// Could use a more specific type, but not super important at the application layer
type Error = anyhow::Error;

// normally should use the url crate, but this is less restrictive for the exercise
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Url(String);

pub struct IndexerConfig {
    /// How many fetch requests to run simultaneously.
    ///
    /// Higher values may help better utilize network bandwidth, while lower values will help
    /// constrain memory usage.
    fetch_concurrency: usize,

    /// How long to wait on a fetch request before giving up
    fetch_timeout: Duration,

    /// How many times to attempt a fetch when encountering a retriable error
    fetch_retries: usize,

    /// How many fetched pages to index simultaneously.
    ///
    /// This is separate from `fetch_concurrency` because fetching is typically IO constrained,
    /// while indexing is expected to be CPU constrained. Putting CPU bound work on the IO workers
    /// can lead to undesirable latency, so the indexing will be run on a separately configured
    /// thread pool
    index_concurrency: usize,
}

impl Default for IndexerConfig {
    fn default() -> Self {
        Self {
            fetch_concurrency: 10_000,
            fetch_timeout: Duration::from_secs(2),
            fetch_retries: 3,
            index_concurrency: std::thread::available_parallelism()
                .map(|x| x.get())
                .unwrap_or(8),
        }
    }
}

pub struct Indexer<S> {
    /// The page where the crawl should begin
    origin: Url,

    /// How far from the origin should indexing search
    max_depth: usize,

    config: IndexerConfig,
    http_service: S,
    link_finder: LinkFinder,
}

impl<S> Indexer<S> {
    pub fn new(origin: Url, max_depth: usize, config: IndexerConfig, http_service: S) -> Self {
        let mut link_finder = LinkFinder::new();
        link_finder.kinds(&[linkify::LinkKind::Url]);
        Self {
            origin,
            max_depth,
            config,
            http_service,
            link_finder,
        }
    }

    fn find_links<'p>(&self, page: &'p PageBody) -> impl Iterator<Item = Url> + 'p {
        self.link_finder
            .links(&page.0)
            .map(|link_string| Url(link_string.as_str().to_owned()))
    }
}

impl<S: HttpService + Send + Sync + 'static> Indexer<S> {
    /// Fetch a single web page at the given url
    async fn fetch(&self, url: &Url) -> Result<PageBody, Error> {
        for remaining_retries in (0..usize::max(1, self.config.fetch_retries)).rev() {
            return match tokio::time::timeout(
                self.config.fetch_timeout,
                self.http_service.fetch(url),
            )
            .await
            {
                Ok(Ok(body)) => Ok(body),

                // if the request timed out, or encountered a retriable error, loop back to retry
                // if possible
                Err(TimeoutElapsed { .. }) | Ok(Err(FetchError::RetriableError(_)))
                    if remaining_retries > 0 =>
                {
                    continue
                }

                // otherwise errors are terminal
                Err(err) => Err(err.into()),
                Ok(Err(err)) => Err(err.into()),
            };
        }
        unreachable!("match cases are technically exhaustive");
    }

    /// Starting at the indexer's origin, search through the web pages up to the defined maximum
    /// depth, yielding the encountered pages.
    fn crawl<'a>(
        self: &'a Arc<Self>,
    ) -> impl Stream<Item = Result<CrawledPage, Error>> + Send + 'a {
        assert_ne!(self.config.fetch_concurrency, 0);

        struct CrawlTarget {
            url: Url,
            depth: usize,
        }

        struct CrawlResult {
            page: CrawledPage,
            links: Vec<Url>,
        }

        let mut encountered_pages = HashSet::<Url>::new();
        let mut fetch_queue = VecDeque::<CrawlTarget>::new();
        let mut workers = FuturesUnordered::new();

        encountered_pages.insert(self.origin.clone());
        fetch_queue.push_back(CrawlTarget {
            url: self.origin.clone(),
            depth: 0,
        });

        async_stream::stream! {
            loop {
                // populate the concurrent worker set whenever it's below capacity
                let spare_capacity = self.config.fetch_concurrency - workers.len();
                let available_urls = usize::min(spare_capacity, fetch_queue.len());
                for CrawlTarget { url, depth } in fetch_queue.drain(0..available_urls) {
                    let this = Arc::clone(self); // clone to send to `spawn` which requires 'static
                    workers.push(tokio::spawn(async move {
                        let body = match this.fetch(&url).await {
                            Ok(body) => body,
                            Err(_) => {
                                // Should consider how to handle fetch errors. 404 will be
                                // commonplace for example, and should just skip the page. On the
                                // other hand, it's worthwhile propagating errors if the indexer
                                // has some IO error and can't fetch _any_ pages. Could use some
                                // heuristics for actual production
                                // For this exercise, return an empty page
                                PageBody("".into())
                            }
                        };
                        // collect the links in the page to extend the crawl,
                        // but only if within the depth limit
                        let links = if depth < this.max_depth {
                            this.find_links(&body)
                                .collect::<Vec<_>>()
                        } else {
                            vec![]
                        };

                        Ok::<_, Error>(CrawlResult {
                            page: CrawledPage { url, depth, body },
                            links
                        })
                    }));
                }

                // poll the workers until one has completed
                let CrawlResult { page, links } = match workers.next().await {
                    // if the worker queue is empty, the fetch_queue must also be empty.
                    // that means we're done crawling
                    None => {
                        assert!(fetch_queue.is_empty());
                        break
                    }
                    Some(Ok(Ok(result))) => result,
                    Some(Ok(Err(fetch_err))) => {
                        yield Err(fetch_err);
                        continue
                    }
                    Some(Err(background_panic)) => std::panic::resume_unwind(background_panic.into_panic()),
                };

                // populate the fetch queue with links from the new page
                for url in links {
                    let is_new_link = encountered_pages.insert(url.clone());
                    // only push links if not visited
                    if is_new_link {
                        fetch_queue.push_back(CrawlTarget { url, depth: page.depth + 1 });
                    }
                }

                yield Ok(page);
            }
        }
    }

    /// Initiate a web crawl from the origin defined at construction, and index the encountered
    /// pages
    pub async fn index(self) -> Result<Index, Error> {
        let this = Arc::new(self);

        // start the web crawl
        let mut crawler = pin!(this.crawl());

        // use an mpmc (spmc?) channel to distribute work across the worker tasks.
        // It should have some bound to limit memory usage and apply backpressure to the crawler.
        // How much is always a question, guesstimate some fraction of the crawl concurrency
        let (work_sender, work_receiver) =
            flume::bounded(usize::max(1, this.config.fetch_concurrency / 10));

        // spawn the workers. Each one will build an independent index in parallel.
        // Later they will be merged together
        let workers = (0..usize::max(1, this.config.index_concurrency))
            .map(|_worker_id| {
                let work_queue = work_receiver.clone();
                let origin = this.origin.clone();
                async move {
                    tokio::task::spawn_blocking(move || {
                        let mut index = Index::new(origin);
                        while let Ok(page) = work_queue.recv() {
                            index.add(page);
                        }
                        index
                    })
                    .await
                    .unwrap_or_else(|panic| std::panic::resume_unwind(panic.into_panic()))
                }
            })
            .collect::<Vec<_>>();

        // send the crawl results to the workers
        while let Some(page) = crawler.next().await {
            if let Err(_all_receivers_dropped) = work_sender.send_async(page?).await {
                // the only reason receivers should drop before the sender task is if there's
                // some panic. In that case nothing to do on the sender, just give up
                break;
            }
        }

        // once the crawler is exhausted, drop the channel sender to signal worker completion
        mem::drop(work_sender);

        // then await the workers and merge together the indices
        let mut workers = workers.into_iter();
        let mut merged_index = workers.next().expect("spawned at least one worker").await;
        for worker in workers {
            merged_index.merge(worker.await);
        }

        Ok(merged_index)
    }
}

#[derive(Debug)]
#[cfg_attr(test, derive(PartialEq, Eq))]
struct CrawledPage {
    url: Url,
    depth: usize,
    body: PageBody,
}

/// An index of crawled web pages which can be queried with search terms to get relevant pages.
///
/// The underlying implementation records the words encountered in pages using finite state machines
/// for later fuzzy-search by Levenshtein distance. This means that search relevance is evaluated
/// *lexically* and not *semantically*. For example, the words "runner" and "running" will be
/// considered similar due to sharing many letters (more precisely, due to having a small edit
/// distance). The words "runner" and "jogger" will not be considered similar because this algorithm
/// does not consider the semantics of the words.
///
/// This algorithm was chosen for relative simplicity in implementation. Semantic similarity is
/// harder to determine (e.g. by machine learning) and is an area I'm unfamiliar with.
///
/// Although lexical similarity has its shortcomings, a conservative edit distance can support
/// fairly wide use cases common in English writing. Plurals, capitalization, tenses, and some
/// conjugation is covered by relatively small edit distances. For example, the search term "runner"
/// will match "runners" and "Runner" with only an edit distance of 1, and "running" with an edit
/// distance of 3. Typos are also covered, so "rynner" or "eunner" would be similar as well.
/// Notably there is a risk of false positives. "tunnel" for example is only an edit distance of 2
/// away. For this reason, closer matches are given higher scores and the final output is sorted by
/// score. The acceptable edit distance must also account for the length of the search term, so
/// that "run" does not eagerly match the abundance of 3 letter words like "rum" and "fun".
///
/// A small amount of tuning has been applied for this exercise, but a production workload should
/// consider other factors beyond edit distance to get more robust results.
pub struct Index {
    origin: Url,

    /// A compact representation of words with efficient search operations.
    ///
    /// There might be more ergonomic options in the ecosystem (e.g. ones which can store values or
    /// support arbitrary insertions), but this seems to support the operations I need, close
    /// enough
    words: fst::Map<Vec<u8>>,

    /// Due to constraints in the fst data structure, values in the map are limited to 64 bit
    /// ints. To associate arbitrary data with each word, maintain this secondary map of ints to
    /// values
    values: HashMap<u64, Vec<WordOccurence>>,

    /// A regex to find words in a page and ignore other text (punctuation, symbols, etc)
    word_regex: Regex,
}

struct WordOccurence {
    page_url: Arc<Url>,
    depth: usize,
    count_in_page: usize,
}

impl Index {
    fn new(origin: Url) -> Self {
        Index {
            origin,
            words: fst::Map::from_iter(std::iter::empty::<(&[u8], u64)>())
                .expect("empty map should build"),
            values: HashMap::default(),
            word_regex: Regex::new(r"\w+").expect("should be valid regex"),
        }
    }

    /// Add the contents of the given page to this index
    fn add(&mut self, page: CrawledPage) {
        let page_url = Arc::new(page.url);
        let depth = page.depth;

        // find all the words in the page and attach metadata to them
        let mut words_with_metadata = self
            .word_regex
            .find_iter(&page.body.0)
            .map(|m| m.as_str())
            .enumerate()
            .map(|(value_index, word)| {
                (
                    word,
                    value_index as u64,
                    WordOccurence {
                        page_url: Arc::clone(&page_url),
                        depth,
                        count_in_page: 1,
                    },
                )
            })
            .collect::<Vec<(&str, u64, WordOccurence)>>();

        // constructing fst::Map requires elements to be sorted and deduplicated
        words_with_metadata.sort_by_key(|&(word, _, _)| word);
        words_with_metadata.dedup_by(|a, b| {
            if str::eq(a.0, b.0) {
                // perform counting during deduplication for convenience
                // docs say "if same_bucket(a, b) returns true, a is removed" so inc b
                b.2.count_in_page += 1;
                true
            } else {
                false
            }
        });

        // adding individual elements to an fst map isn't possible. Instead, we build an index over
        // the page and then merge the page index into the accumulated index
        let words = fst::Map::from_iter(
            words_with_metadata
                .iter()
                .map(|&(word, value_index, _)| (word, value_index)),
        )
        .expect("words should be sorted and deduped");
        let values = words_with_metadata
            .into_iter()
            .map(|(_, value_index, word_occurence)| (value_index, vec![word_occurence]))
            .collect::<HashMap<_, _>>();

        let page_index = Index {
            origin: self.origin.clone(),
            words,
            values,
            word_regex: self.word_regex.clone(),
        };

        self.merge(page_index);
    }

    /// Merge another index into this one to hold the contents of both
    fn merge(&mut self, that: Self) {
        // although it's possible to merge arbitrary indices, origin tracking is simpler if they
        // always share the same origin
        assert_eq!(
            &self.origin, &that.origin,
            "can only merge indices with a shared origin"
        );

        let mut values = HashMap::with_capacity(self.values.len() + that.values.len());
        let mut value_maps = [mem::take(&mut self.values), that.values];
        let mut word_union = self.words.op().add(&that.words).union();
        let mut next_value_index = 0;
        let words = fst::Map::from_iter(std::iter::from_fn(|| {
            let (word_bytes, index_presence) = word_union.next()?;
            let values_for_word = match index_presence {
                // if only one of the indices have this word, the word's metadata can be taken directly
                &[no_overlap] => value_maps[no_overlap.index]
                    .remove(&no_overlap.value)
                    .expect("value should exist if index is provided"),

                // if both indices contain the word, we must merge the values
                &[idx_a, idx_b] => {
                    let mut values_a = value_maps[idx_a.index].remove(&idx_a.value).unwrap();
                    let mut values_b = value_maps[idx_b.index].remove(&idx_b.value).unwrap();
                    values_a.append(&mut values_b);
                    values_a
                }
                _ => unreachable!("words must be from one or two maps, no more no less"),
            };
            next_value_index += 1;
            values.insert(next_value_index, values_for_word);
            Some((word_bytes.to_owned(), next_value_index))
        }))
        .expect("words should be sorted and deduped");
        mem::drop(word_union);

        self.words = words;
        self.values = values;
    }

    /// Search the index for pages which are relevant to the text in the given query.
    ///
    /// The returned outputs will be sorted descending by an internal relevancy score
    pub fn search<'a>(
        &'a self,
        query: &str,
    ) -> Result<impl Iterator<Item = SearchResult<'a>>, Error> {
        let mut page_scores = BTreeMap::<&Url, (f32, SearchResult)>::new();
        let search_words = self.word_regex.find_iter(query).map(|m| m.as_str());

        // for each word in the query, find pages which have similar words
        for word in search_words {
            let edit_distance = match word.len() {
                // no edit distance for small words, must match exactly
                0..=3 => 0,
                4 => 1,
                5 => 2,
                6 => 3,
                7.. => 4,
                // could consider tuning these more, but that's enough for now
            };
            let fuzzy_target = fst::automaton::Levenshtein::new(word, edit_distance)?;
            let mut search = self.words.search(fuzzy_target).into_stream();
            while let Some((matched_word, value_index)) = search.next() {
                let exact_match = matched_word.eq(word.as_bytes());
                for word_occurence in &self.values[&value_index] {
                    // calculate a score for how well each page matches this word.
                    // Start with a baseline that is high for exact matches and lower for edit
                    // distanced
                    let mut score = if exact_match {
                        3.0
                    } else {
                        // could be relative to edit distance, but this is getting complicated
                        // enough as it is
                        1.0
                    };

                    // if the query word shows up more than once in the page, give additional
                    // score. This has a cap though, to prevent outliers or "SEO"
                    let match_count = usize::min(3, word_occurence.count_in_page) as f32;
                    score *= 1.0 + (match_count / 10.0);

                    page_scores
                        .entry(&word_occurence.page_url)
                        .or_insert((
                            0.0,
                            SearchResult {
                                origin: &self.origin,
                                page: &word_occurence.page_url,
                                depth: word_occurence.depth,
                            },
                        ))
                        .0 += score;
                }
            }
        }

        let mut page_scores = page_scores.into_values().collect::<Vec<_>>();
        page_scores.sort_by(|(a, _), (b, _)| f32::total_cmp(b, a));
        Ok(page_scores.into_iter().map(|(_score, result)| result))
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct SearchResult<'a> {
    pub origin: &'a Url,
    pub page: &'a Url,
    pub depth: usize,
}

/// The contents of a web page.
///
/// Under normal circumstances this would be some byte payload and would need parsing based on
/// content type (e.g. html). For this exercise, let's suppose all bodies are just plain text
#[derive(Debug)]
#[cfg_attr(test, derive(PartialEq, Eq))]
pub struct PageBody(String);

pub trait HttpService {
    /// Read the web page at the given URL and return its text contents.
    fn fetch(&self, url: &Url) -> impl Future<Output = Result<PageBody, FetchError>> + Send;
}

/// Possible errors an HTTP fetch could return
// in real code this would use an http lib's error type, but for the sake of exercise this is
// simpler
#[derive(Debug, thiserror::Error)]
pub enum FetchError {
    #[error("{0}")]
    TerminalError(String),
    #[error("{0}")]
    RetriableError(String),
}

#[cfg(test)]
mod test {
    use super::*;
    use futures::{future, stream::TryStreamExt};
    use std::collections::HashMap;

    macro_rules! map {
        ($($key:expr => $val:expr),*$(,)?) => {
            {
                let mut map = HashMap::new();
                $(
                    map.insert($key, $val);
                )*
                map
            }
        };
    }

    impl<S> HttpService for HashMap<S, S>
    where
        S: std::hash::Hash + PartialEq + Eq + std::borrow::Borrow<str>,
    {
        fn fetch(&self, url: &Url) -> impl Future<Output = Result<PageBody, FetchError>> + Send {
            future::ready(Ok(PageBody(match self.get(&url.0) {
                Some(string) => string.borrow().to_owned(),
                None => String::new(),
            })))
        }
    }

    #[test]
    fn find_links() {
        let indexer = Indexer::new(
            Url("http://localhost".into()),
            0,
            IndexerConfig::default(),
            (),
        );

        assert_eq!(
            indexer
                .find_links(&PageBody(
                    "this is some text http://link1 \
                    other text https://link2.us \
                    http://a \
                    www.notAUrlDueToLackOfScheme.com https://www.weather.gov"
                        .into()
                ))
                .collect::<Vec<_>>(),
            vec![
                Url("http://link1".into()),
                Url("https://link2.us".into()),
                Url("http://a".into()),
                Url("https://www.weather.gov".into()),
            ]
        );
    }

    #[tokio::test]
    async fn crawl_depth() -> Result<(), Error> {
        let web = map! {
            "http://a" => "look at this other site http://b",
            "http://b" => "you can only reach http://c with depth >= 2",
            "http://c" => "end of the path, not reachable with depth 1",
        };

        let max_depth = 1;
        let indexer = Arc::new(Indexer::new(
            Url("http://a".into()),
            max_depth,
            IndexerConfig::default(),
            web,
        ));

        assert_eq!(
            indexer.crawl().try_collect::<Vec<_>>().await?,
            vec![
                CrawledPage {
                    url: Url("http://a".into()),
                    depth: 0,
                    body: PageBody("look at this other site http://b".into())
                },
                CrawledPage {
                    url: Url("http://b".into()),
                    depth: 1,
                    body: PageBody("you can only reach http://c with depth >= 2".into())
                }
            ]
        );
        Ok(())
    }

    #[tokio::test]
    async fn crawl_fanout_and_no_duplicates() -> Result<(), Error> {
        let web = map! {
            "http://a" => "look at these sites http://b, http://c, http://d",
            "http://b" => "another http://e but don't visit http://c again",
            "http://c" => "text C",
            "http://d" => "text D; self loop should be benign http://d",
            "http://e" => "text E",
        };

        let max_depth = 2;
        let indexer = Arc::new(Indexer::new(
            Url("http://a".into()),
            max_depth,
            IndexerConfig::default(),
            web,
        ));

        assert_eq!(
            indexer.crawl().try_collect::<Vec<_>>().await?,
            vec![
                CrawledPage {
                    url: Url("http://a".into()),
                    depth: 0,
                    body: PageBody("look at these sites http://b, http://c, http://d".into())
                },
                CrawledPage {
                    url: Url("http://b".into()),
                    depth: 1,
                    body: PageBody("another http://e but don't visit http://c again".into())
                },
                CrawledPage {
                    url: Url("http://c".into()),
                    depth: 1,
                    body: PageBody("text C".into())
                },
                CrawledPage {
                    url: Url("http://d".into()),
                    depth: 1,
                    body: PageBody("text D; self loop should be benign http://d".into())
                },
                CrawledPage {
                    url: Url("http://e".into()),
                    depth: 2,
                    body: PageBody("text E".into())
                },
            ]
        );
        Ok(())
    }

    #[test]
    fn search_index() -> Result<(), Error> {
        let origin = &Url("http://root".into());
        let mut index = Index::new(origin.clone());
        index.add(CrawledPage {
            url: Url("http://exactmatch".into()),
            depth: 1,
            body: PageBody("runner".into()),
        });
        index.add(CrawledPage {
            url: Url("http://added.ing".into()),
            depth: 1,
            body: PageBody("running".into()),
        });
        index.add(CrawledPage {
            url: Url("http://Capitalize".into()),
            depth: 1,
            body: PageBody("Runner".into()),
        });
        index.add(CrawledPage {
            url: Url("http://nomatch".into()),
            depth: 1,
            body: PageBody("no match".into()),
        });
        index.add(CrawledPage {
            url: Url("http://falsepositive".into()),
            depth: 1,
            body: PageBody("tunnel".into()),
        });
        index.add(CrawledPage {
            url: Url("http://exactmatchtwice".into()),
            depth: 1,
            body: PageBody("runner runner".into()),
        });

        assert_eq!(
            index.search("runner")?.collect::<Vec<_>>(),
            vec![
                SearchResult {
                    origin,
                    page: &Url("http://exactmatchtwice".into()),
                    depth: 1,
                },
                SearchResult {
                    origin,
                    page: &Url("http://exactmatch".into()),
                    depth: 1,
                },
                SearchResult {
                    origin,
                    page: &Url("http://Capitalize".into()),
                    depth: 1,
                },
                SearchResult {
                    origin,
                    page: &Url("http://added.ing".into()),
                    depth: 1,
                },
                SearchResult {
                    origin,
                    page: &Url("http://falsepositive".into()),
                    depth: 1,
                },
            ]
        );

        Ok(())
    }

    #[tokio::test]
    async fn end_to_end() -> Result<(), Error> {
        let web = map! {
            "http://running.com" => "This site is all about running, because that's the analogy I used for lexical vs semantic similarity. \
                If you consider yourself runner, you've found the right place. \
                If you prefer things slower, consider http://jogging.us",
            "http://jogging.us" => "This site is all about jogging, joggers, keeping healthy but not too strenuous. \
                Those guys over at http://running.com are too fast for our taste, but consider also visiting \
                http://bikes.com",
            "http://bikes.com" => "We've got bikes and bike accessories",
            "http://unreachable.gov" => "Nobody visits us"
        };

        let index_from_running = Indexer::new(
            Url("http://running.com".into()),
            5,
            IndexerConfig::default(),
            web,
        )
        .index()
        .await?;

        assert_eq!(
            index_from_running
                .search("Where to buy a bike")?
                .collect::<Vec<_>>(),
            vec![
                SearchResult {
                    origin: &Url("http://running.com".into()),
                    page: &Url("http://bikes.com".into()),
                    depth: 2,
                },
                // jogging is included because the bikes.com is part of its index
                SearchResult {
                    origin: &Url("http://running.com".into()),
                    page: &Url("http://jogging.us".into()),
                    depth: 1,
                }
            ]
        );

        Ok(())
    }
}
